﻿using UnityEngine;
using UnityEngine.AI;

namespace AI.Movement.Pathfinding
{
	public class NavMeshEditing : MonoBehaviour
	{
		public Transform from;
		public Transform to;

		private NavMeshPath path;
		void Update()
		{
			// Use the navmesh pathfinder to obtain a path
			var filter = new NavMeshQueryFilter();
			filter.areaMask = NavMesh.AllAreas;
			filter.agentTypeID = 0;
			path = new NavMeshPath();
			NavMesh.CalculatePath(from.position, to.position, filter, path);
			//Debug.LogError("FOUND PATH: " + path.corners.Length);
			
			for (var i = 0; i < path.corners.Length-1; i++)
			{
				var cornerFrom = path.corners[i];
				var cornerTo = path.corners[i+1];
				Debug.DrawLine(cornerFrom, cornerTo, Color.Lerp(Color.yellow, Color.red, i * 1f / path.corners.Length));
			}

			NavMesh.SetAreaCost(5, 0);
		}
	}
}