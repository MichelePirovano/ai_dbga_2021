using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI.Movement.Pathfinding
{
	public class NavmeshTarget : MonoBehaviour
	{
		public NavMeshAgent[] Agents;

		void Start()
		{
			Agents = FindObjectsOfType<NavMeshAgent>();
		}
		
		private void Update()
		{
			
			if (Input.GetMouseButtonDown(0))
			{
				var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				var dir = Camera.main.transform.forward;

				Debug.DrawRay(mouseWorldPos, dir * 100, Color.red, 1f);

				RaycastHit hit;
				var hasHit = Physics.Raycast(mouseWorldPos, dir, out hit);

				if (hasHit)
				{
					Debug.DrawLine(mouseWorldPos, hit.point, Color.green, 1f);
					transform.position = hit.point;
					foreach (var Agent in Agents)
					{
						Agent.SetDestination(hit.point);

						var path = Agent.path;
						for (var i = 0; i < path.corners.Length-1; i++)
						{
							var cornerFrom = path.corners[i];
							var cornerTo = path.corners[i+1];
							Debug.DrawLine(cornerFrom, cornerTo, Color.Lerp(Color.yellow, Color.red, i * 1f / path.corners.Length), 5f);
						}
					}
				}
				

				
			}
			
		}
	}

	
}