using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
    public class DepthFirstSearch : MonoBehaviour
    {
        public GraphMaker GraphMaker;

        public Node StartNode;
        public Node EndNode;

        public float Step;
        
        public IEnumerator Start()
        {
            StartNode.Color = Color.green;
            EndNode.Color = Color.blue;
            
            List<Node> visitedNodes = new List<Node>();
            Stack<Node> stack = new Stack<Node>();
            stack.Push(StartNode);
            visitedNodes.Add(StartNode);
            StartNode.Color = Color.yellow;
            yield return new WaitForSeconds(Step);

            while (stack.Count > 0)
            {
                Node current = stack.Pop();
                current.Color = Color.green;
                yield return new WaitForSeconds(Step);

                if (current == EndNode)
                {
                    break;
                }

                var neighbours = GraphMaker.GetNeighbours(current);

                // Example of sorting, if we want to change how the neighbours are traversed (not necessary)
                // neighbours.Sort((a, b) => (int)(b.transform.position.x - a.transform.position.x));            
                
                foreach (var neigh in neighbours)
                {
                    if (visitedNodes.Contains(neigh)) continue;
                    stack.Push(neigh);
                    visitedNodes.Add(neigh);
                    neigh.Color = Color.yellow;
                    yield return new WaitForSeconds(Step);
                }
                current.Color = Color.red;
            }
        }
    }

}