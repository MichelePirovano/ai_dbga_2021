using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
    public class BreadthFirstSearch : MonoBehaviour
    {
        public GraphMaker GraphMaker;

        public Node StartNode;
        public Node EndNode;

        public float Step;
        
        public IEnumerator Start()
        {
            StartNode.Color = Color.green;
            EndNode.Color = Color.blue;
            
            List<Node> visitedNodes = new List<Node>();
            Queue<Node> queue = new Queue<Node>();
            queue.Enqueue(StartNode);
            visitedNodes.Add(StartNode);
            StartNode.Color = Color.yellow;
            yield return new WaitForSeconds(Step);

            while (queue.Count > 0)
            {
                Node current = queue.Dequeue();
                current.Color = Color.red;
                yield return new WaitForSeconds(Step);

                if (current == EndNode)
                {
                    break;
                }

                foreach (var neigh in GraphMaker.GetNeighbours(current))
                {
                    if (visitedNodes.Contains(neigh)) continue;
                    queue.Enqueue(neigh);
                    visitedNodes.Add(neigh);
                    neigh.Color = Color.yellow;
                    yield return new WaitForSeconds(Step);
                }
            }
        }
    }

}