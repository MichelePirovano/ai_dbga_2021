using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
	public class Node : MonoBehaviour
	{
		public SpriteRenderer sr;
		public TextMesh tm;

		public Color Color = Color.white;
		public float Value;
		public int Danger;
		
		public void Draw()
		{
			tm.text = Value.ToString("F");
			sr.color = Color;
			//sr.transform.localScale = Danger * Vector3.one * 0.5f;
		}
		
	}

}
