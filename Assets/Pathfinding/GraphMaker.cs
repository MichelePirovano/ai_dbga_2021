﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
	public class GraphMaker : MonoBehaviour
	{
		public float Radius;

		public List<Node> nodes;
		public List<Edge> edges;

		public bool Visible = true;
		
		public List<Node> GetNeighbours(Node fromNode)
		{
			List<Node> neighs = new List<Node>();

			foreach (var edge in edges)
			{
				if (edge.NodeA == fromNode)
				{
					neighs.Add(edge.NodeB);
				}
				if (edge.NodeB == fromNode)
				{
					neighs.Add(edge.NodeA);
				}
			}
			
			return neighs;
		}

		void Awake()
		{
			Regenerate();
		}
		
		public void Regenerate()
		{
			nodes = FindObjectsOfType<Node>().ToList();
			edges = new List<Edge>();
			
			
			// Assign a "Danger" value to each node
			/*foreach (var node in nodes)
			{
				node.Danger = Random.Range(0, 10);
			}*/

			foreach (var nodeA in nodes)
			{
				foreach (var nodeB in nodes)
				{
					if (nodeA == nodeB) continue;

					if (Vector2.SqrMagnitude(nodeA.transform.position - nodeB.transform.position) < Radius * Radius)
					{
						var edgeGO = new GameObject("Edge");
						var edge = edgeGO.AddComponent<Edge>();
						edge.NodeA = nodeA;
						edge.NodeB = nodeB;
						edge.SideOffset = 0f;
						edge.DirectionOffset = 0.2f;

						if (!edges.Contains(edge))
						{
							edges.Add(edge);
						}

						/*
						bool addIt = true;
						foreach (var edge1 in edges)
						{
							if (edge1.NodeA == edge.NodeB
							    && edge1.NodeB == edge.NodeA)
							{
								addIt = false;
								break;
							}
						}
						
						if (addIt) edges.Add(edge);*/
					}
				}
			}
		}

		void Update()
		{
			if (!Visible)
			{
				foreach (var node in nodes) node.gameObject.SetActive(false);
			}
			else
			{
				foreach (var node in nodes) node.gameObject.SetActive(true);
				foreach (var node in nodes) node.Draw();
				foreach (var edge in edges) edge.Draw();
			}
		}
		
	}
}