using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
    public class AStarSearch : MonoBehaviour
    {
        public GraphMaker GraphMaker;

        public Node StartNode;
        public Node EndNode;

        public float Step;
        public float HeuristicRatio;

        public bool AutoStart;
        public void Start()
        {
            if (AutoStart) FindPath(new List<Edge>());
        }

        public void FindPath(List<Edge> path)
        {
            StartCoroutine(FindPathCoroutine(path));
        }

        
        public bool IsExecuting;
        public IEnumerator FindPathCoroutine(List<Edge> path)
        {
            IsExecuting = true;
            StartNode.Color = Color.green;
            EndNode.Color = Color.blue;
            
            List<Node> closedSet = new List<Node>();
            List<Node> openSet = new List<Node>();
            
            // Initialise values
            foreach (var node in GraphMaker.nodes) node.Value = Mathf.Infinity;
            StartNode.Value = 0;

            openSet.Add(StartNode);
            StartNode.Color = Color.yellow;
            yield return new WaitForSeconds(Step);

            bool pathFound = false;
            while (openSet.Count > 0)
            {
                // Extract the best current node
                // A* also takes into account the heuristic value
                Node current = null;
                var minValue = Mathf.Infinity;
                foreach (var node in openSet)
                {
                    var heuristic = Vector2.Distance(node.transform.position, EndNode.transform.position);
                    var totalValue = node.Value + heuristic * HeuristicRatio;
                    
                    if (totalValue < minValue)
                    {
                        minValue = totalValue;
                        current = node;
                    }
                }
                openSet.Remove(current);
                
                current.Color = Color.red;
                current.Color = Color.Lerp(Color.yellow, Color.red, current.Value / 10f);
                yield return new WaitForSeconds(Step);

                if (current == EndNode)
                {
                    pathFound = true;
                    break;
                }
                
                closedSet.Add(current);

                foreach (var neigh in GraphMaker.GetNeighbours(current))
                {
                    // Add the new node if it is new
                    if (!closedSet.Contains(neigh) && !openSet.Contains(neigh))
                    {
                        openSet.Add(neigh);
                        neigh.Color = Color.yellow;
                    }

                    // Compute the cost of the segment
                    var segmentCost = SegmentCost(current, neigh);

                    // Update the value with the minimum
                    var newValue = current.Value + segmentCost;
                    if (newValue < neigh.Value)
                    {
                        neigh.Value = newValue;
                        neigh.Color = Color.Lerp(Color.yellow, Color.red, neigh.Value / 10f);
                        
                        // Find the edge that connects the two nodes
                        foreach (var edge in GraphMaker.edges)
                        {
                            if ((edge.NodeA == neigh && edge.NodeB == current)
                                || (edge.NodeB == neigh && edge.NodeA == current))
                            {
                                edge.Color = neigh.Color;
                                break;
                            }
                        }
                        
                    }
                    
                    yield return new WaitForSeconds(Step);
                }
            }


            if (pathFound)
            {
                Debug.Log("Found a path!");

                Node currentNode = EndNode;
                currentNode.Color = Color.green;
                while (currentNode != StartNode)
                {
                    // Find the neighbour that moves us to the shortest part
                    var neighs = GraphMaker.GetNeighbours(currentNode);
                    Node bestNeigh = null;
                    var minValue = Mathf.Infinity;
                    foreach (var neigh in neighs)
                    {
                        var segmentCost = SegmentCost(currentNode, neigh); 
                        
                        var totalCost = neigh.Value + segmentCost;
                        if (totalCost < minValue)
                        {
                            minValue = totalCost;
                            bestNeigh = neigh;
                        }
                    }

                    bestNeigh.Color = Color.green;
                    yield return new WaitForSeconds(Step);
                    
                    // Find the edge that connects the two nodes
                    foreach (var edge in GraphMaker.edges)
                    {
                        if ((edge.NodeA == currentNode && edge.NodeB == bestNeigh)
                            || (edge.NodeB == currentNode && edge.NodeA == bestNeigh))
                        {
                            path.Add(edge);
                            edge.Color = Color.green;
                            yield return new WaitForSeconds(Step);
                            break;
                        }
                    }
                    
                    // Move to the next neigh
                    currentNode = bestNeigh;
                }

                // Reverse the path so that the start node is at the first edge
                path.Reverse();
                
                // Color based on path index
                for (var i = 0; i < path.Count; i++)
                {
                    var edge = path[i];
                    edge.Color = Color.Lerp(Color.green, Color.blue, i * 1f / path.Count);
                }
            }
            
        }

        private float SegmentCost(Node n1, Node n2)
        {
            return Vector2.Distance(n1.transform.position, n2.transform.position);// + (n1.Danger*n1.Danger + n2.Danger*n2.Danger)* 10;
        }
    }

}