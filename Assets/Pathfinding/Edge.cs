﻿using UnityEngine;

namespace AI.Movement.Pathfinding
{
	public class Edge : MonoBehaviour
	{
		public Node NodeA;
		public Node NodeB;
		
		public Color Color = Color.white;

		public float SideOffset;
		public float DirectionOffset;
		
		public void Draw()
		{
			var direction = (NodeB.transform.position - NodeA.transform.position).normalized;
			var cross = Vector3.Cross(direction, Vector3.forward).normalized;
			
			Debug.DrawLine(
				NodeA.transform.position + cross * SideOffset + direction * DirectionOffset,
				NodeB.transform.position + cross * SideOffset - direction * DirectionOffset,
				Color);
		}

		public override bool Equals(object other)
		{
			var otherEdge = (Edge) other;
			if (otherEdge == null) return false;
			
			return (NodeA == otherEdge.NodeA && NodeB == otherEdge.NodeB)
			       ||
			       (NodeA == otherEdge.NodeB && NodeB == otherEdge.NodeA);
		}
	}
}