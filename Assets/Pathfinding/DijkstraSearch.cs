using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Movement.Pathfinding
{
    public class DijkstraSearch : MonoBehaviour
    {
        public GraphMaker GraphMaker;

        public Node StartNode;
        public Node EndNode;

        public float Step;
        
        public IEnumerator Start()
        {
            StartNode.Color = Color.green;
            EndNode.Color = Color.blue;
            
            List<Node> closedSet = new List<Node>();
            List<Node> openSet = new List<Node>();
            
            // Initialise values
            foreach (var node in GraphMaker.nodes) node.Value = Mathf.Infinity;
            StartNode.Value = 0;

            openSet.Add(StartNode);
            StartNode.Color = Color.yellow;
            yield return new WaitForSeconds(Step);

            bool pathFound = false;
            while (openSet.Count > 0)
            {
                // Extract the best current node
                Node current = null;
                var minValue = Mathf.Infinity;
                foreach (var node in openSet)
                {
                    if (node.Value < minValue)
                    {
                        minValue = node.Value;
                        current = node;
                    }
                }
                openSet.Remove(current);
                
                current.Color = Color.red;
                current.Color = Color.Lerp(Color.yellow, Color.red, current.Value / 10f);
                yield return new WaitForSeconds(Step);

                if (current == EndNode)
                {
                    pathFound = true;
                    break;
                }
                
                closedSet.Add(current);

                foreach (var neigh in GraphMaker.GetNeighbours(current))
                {
                    // Add the new node if it is new
                    if (!closedSet.Contains(neigh) && !openSet.Contains(neigh))
                    {
                        openSet.Add(neigh);
                        neigh.Color = Color.yellow;
                    }

                    // Compute the cost of the segment
                    var segmentCost = Vector2.Distance(
                        current.transform.position,
                        neigh.transform.position);

                    // Update the value with the minimum
                    var newValue = current.Value + segmentCost;
                    if (newValue < neigh.Value)
                    {
                        neigh.Value = newValue;
                        neigh.Color = Color.Lerp(Color.yellow, Color.red, neigh.Value / 10f);
                        
                        // Find the edge that connects the two nodes
                        foreach (var edge in GraphMaker.edges)
                        {
                            if ((edge.NodeA == neigh && edge.NodeB == current)
                                || (edge.NodeB == neigh && edge.NodeA == current))
                            {
                                edge.Color = neigh.Color;
                                break;
                            }
                        }
                        
                    }
                    
                    yield return new WaitForSeconds(Step);
                }
            }


            if (pathFound)
            {
                Debug.Log("Found a path!");

                List<Edge> path = new List<Edge>();
                Node currentNode = EndNode;
                currentNode.Color = Color.green;
                while (currentNode != StartNode)
                {
                    // Find the neighbour that moves us to the shortest part
                    var neighs = GraphMaker.GetNeighbours(currentNode);
                    Node bestNeigh = null;
                    var minValue = Mathf.Infinity;
                    foreach (var neigh in neighs)
                    {
                        var segmentCost = Vector2.Distance(currentNode.transform.position, neigh.transform.position);
                        var totalCost = neigh.Value + segmentCost;
                        if (totalCost < minValue)
                        {
                            minValue = totalCost;
                            bestNeigh = neigh;
                        }
                    }

                    bestNeigh.Color = Color.green;
                    yield return new WaitForSeconds(Step);
                    
                    // Find the edge that connects the two nodes
                    foreach (var edge in GraphMaker.edges)
                    {
                        if ((edge.NodeA == currentNode && edge.NodeB == bestNeigh)
                            || (edge.NodeB == currentNode && edge.NodeA == bestNeigh))
                        {
                            path.Add(edge);
                            edge.Color = Color.green;
                            yield return new WaitForSeconds(Step);
                            break;
                        }
                    }
                    
                    // Move to the next neigh
                    currentNode = bestNeigh;
                }

            }
            
        }
    }

}