using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Genetics
{
	public class Genotype
	{
		public int color; 

		public int frontMagnitudo = 1; 
		public int backMagnitudo = 1;

		public int frontFrequency = 1;
		public int backFrequency = 1;

		public int frequencyFrequency = 1;

		public int frontLegLength = 20;
		public int backLegLength = 20;


		public float fitness;
	}
    
	public class Individual : MonoBehaviour
	{
		public HingeJoint2D jointFront;
		public HingeJoint2D jointBack;


		public Transform body;
		public Transform backLeg;
		public Transform frontLeg;
		
		public float frontMagnitudo = 1f;
		public float frontFrequency = 1f;

		public float frequencyFrequency;
		
		public float backMagnitudo = 1f;
		public float backFrequency = 1f;

		private float t = 0.0f;

		/*private void Start()
		{
			var g = new Genotype();
			g.color = 50;
			g.frontLegLength = UnityEngine.Random.Range(1, 20);
			g.backLegLength = UnityEngine.Random.Range(1, 20);
			SetGenotype(g);
		}*/

		public float fitness;
		public float maxX;
		public float maxY;
		public float maxSpeedX;
		public float maxRotZ;

		void FixedUpdate()
		{
			t += Time.fixedDeltaTime;
			jointFront.useMotor = true;
			var motorFront = jointFront.motor;

			var fFrequency = frontFrequency * Mathf.Sin(t * frequencyFrequency);
			var bFrequency = backFrequency * Mathf.Sin(t * frequencyFrequency);
			
			motorFront.motorSpeed = frontMagnitudo * Mathf.Sin(t * fFrequency);
			jointFront.motor = motorFront;
			
			jointBack.useMotor = true;
			var motorBack = jointBack.motor;
			motorBack.motorSpeed = backMagnitudo * Mathf.Cos(t * bFrequency);
			jointBack.motor = motorBack;

			maxX = Mathf.Max(maxX, body.transform.position.x);
			maxY = Mathf.Max(maxY, body.transform.position.y);

			maxSpeedX = Mathf.Max(maxSpeedX, body.GetComponent<Rigidbody2D>().velocity.x);
			maxRotZ = Mathf.Max(maxRotZ, Mathf.Abs(body.transform.localEulerAngles.z));

			fitness = maxSpeedX * 10 + maxX;
		}


		public Genotype genotype;
		public void SetGenotype(Genotype genotype)
		{
			this.genotype = genotype;

			var srs = GetComponentsInChildren<SpriteRenderer>();
			foreach (var sr in srs)
			{
				sr.color = Color.HSVToRGB(genotype.color / 100f, 1, 1);
			}

			frontMagnitudo = genotype.frontMagnitudo/10f;
			backMagnitudo = genotype.backMagnitudo/10f;
			
			frontFrequency = genotype.frontFrequency/10f;
			backFrequency = genotype.backFrequency/10f;
			
			frequencyFrequency = genotype.frequencyFrequency/10f;

			var backLegLength = genotype.backLegLength/10f;
			backLeg.transform.localScale = new Vector3(backLegLength, backLeg.transform.localScale.y, 1);
			backLeg.transform.localPosition += Vector3.down * (backLegLength-1) / 2f;

			var frontLegLength = genotype.frontLegLength/10f;
			frontLeg.transform.localScale = new Vector3(frontLegLength, frontLeg.transform.localScale.y, 1);
			frontLeg.transform.localPosition += Vector3.down * (frontLegLength-1) / 2f;

		}

	}

}
