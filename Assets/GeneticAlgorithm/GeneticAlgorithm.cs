using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Genetics
{
	public class GeneticAlgorithm : MonoBehaviour
	{
		public Individual prefabIndividual;
		public float SimulationSeconds;
		
		public int NGenerations = 1;
		public int PopulationSize = 10;
		
		public int minFrequency = 0;
		public int maxFrequency = 1000;

		public int minMagnitudo = -100;
		public int maxMagnitudo = +100;

		public int minLegLength = 1;
		public int maxLegLenght = 20;

		public int minColor = 0;
		public int maxColor = 100;


		public float TruncationPercentage = 0.3f;
		public int NMutations = 1;
		
		public float SpeedUpTimescale = 5f;
		
		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Alpha1)) Time.timeScale = 1f;
			if (Input.GetKeyDown(KeyCode.Alpha2)) Time.timeScale = SpeedUpTimescale;
			
		}
		
		IEnumerator Start()
		{
			// Create the initial population genotypes
			List<Genotype> initialGenotypes = new List<Genotype>();
			// ... we create a random population so we don't skip good candidates

			for (int i = 0; i < PopulationSize; i++)
			{
				var g = new Genotype();
				g.backFrequency = Random.Range(minFrequency, maxFrequency+1);
				g.frontFrequency = Random.Range(minFrequency, maxFrequency+1);
				g.backMagnitudo = Random.Range(minMagnitudo, maxMagnitudo + 1);
				g.frontMagnitudo = Random.Range(minMagnitudo, maxMagnitudo + 1);
				g.backLegLength = Random.Range(minLegLength, maxLegLenght + 1);
				g.frontLegLength = Random.Range(minLegLength, maxLegLenght + 1);
				g.frequencyFrequency = Random.Range(minFrequency, maxFrequency+1);
				g.color = Random.Range(minColor, maxColor + 1);
				initialGenotypes.Add(g);
			}

			var currentGenotypes = initialGenotypes;
			
			// For each generation
			for (int iGeneration = 0; iGeneration < NGenerations; iGeneration++)
			{
				// Spawn the population
				List<Individual> population = new List<Individual>();
				for (int iIndividual = 0; iIndividual < currentGenotypes.Count; iIndividual++)
				{
					var spawnedIndividual = Instantiate(prefabIndividual.gameObject).GetComponent<Individual>();
					spawnedIndividual.SetGenotype(currentGenotypes[iIndividual]);
					spawnedIndividual.transform.position = new Vector3(0, 0, iIndividual);
					population.Add(spawnedIndividual);
				}
				
				// Simulate the world...
				yield return new WaitForSeconds(SimulationSeconds);
				
				// Evaluate the fitness
				foreach (var individual in population)
				{
					individual.genotype.fitness = individual.fitness;
					Destroy(individual.gameObject);
				}
				
				
				// Selection: truncation
				currentGenotypes.Sort((x,y) => (int)( 100*y.fitness - 100*x.fitness));
				var selectedGenotypes = currentGenotypes.GetRange(0, (int) (TruncationPercentage * PopulationSize));
				
				// Crossover
				var crossoverGenotypes = new List<Genotype>();
				for (int iPair = 0; iPair < selectedGenotypes.Count/2; iPair++)
				{
					var p1 = selectedGenotypes[iPair * 2];
					var p2 = selectedGenotypes[iPair * 2 + 1];
					
					Genotype c1 = new Genotype();
					c1.backFrequency = Crossover(p1.backFrequency, p2.backFrequency);
					c1.frontFrequency = Crossover(p1.frontFrequency, p2.frontFrequency);
					c1.backMagnitudo = Crossover(p1.backMagnitudo, p2.backMagnitudo);
					c1.frontMagnitudo = Crossover(p1.frontMagnitudo, p2.frontMagnitudo);
					c1.backLegLength = Crossover(p1.backLegLength, p2.backLegLength);
					c1.frontLegLength = Crossover(p1.frontLegLength, p2.frontLegLength);
					c1.frontFrequency = Crossover(p1.frontFrequency, p2.frontFrequency);
					c1.color = Crossover(p1.color, p2.color);
					
					Genotype c2 = new Genotype();
					c2.backFrequency = Crossover(p1.backFrequency, p2.backFrequency);
					c2.frontFrequency = Crossover(p1.frontFrequency, p2.frontFrequency);
					c2.backMagnitudo = Crossover(p1.backMagnitudo, p2.backMagnitudo);
					c2.frontMagnitudo = Crossover(p1.frontMagnitudo, p2.frontMagnitudo);
					c2.backLegLength = Crossover(p1.backLegLength, p2.backLegLength);
					c2.frontLegLength = Crossover(p1.frontLegLength, p2.frontLegLength);
					c2.frontFrequency = Crossover(p1.frontFrequency, p2.frontFrequency);
					c2.color = Crossover(p1.color, p2.color);

					crossoverGenotypes.Add(c1);
					crossoverGenotypes.Add(c2);
				}
				
				
				// Mutation
				var mutatedGenotypes = new List<Genotype>();
				var nMutated = PopulationSize - selectedGenotypes.Count - crossoverGenotypes.Count;
				for (int iMutated = 0; iMutated < nMutated; iMutated++)
				{
					var p = crossoverGenotypes[Random.Range(0, crossoverGenotypes.Count)];
					
					var m = new Genotype();
					m.backFrequency = p.backFrequency;
					m.frontFrequency = p.frontFrequency;
					m.backMagnitudo = p.backMagnitudo;
					m.frontMagnitudo = p.frontMagnitudo;
					m.backLegLength = p.backLegLength;
					m.frontLegLength = p.frontLegLength;
					m.frequencyFrequency = p.frequencyFrequency;
					m.color = p.color;

					for (int i = 0; i < NMutations; i++)
					{
						int choice = Random.Range(0, 8);
						switch (choice)
						{
							case 0: m.backFrequency = Random.Range(minFrequency, maxFrequency); break;
							case 1: m.frontFrequency = Random.Range(minFrequency, maxFrequency); break;
							case 2: m.backMagnitudo = Random.Range(minMagnitudo, maxMagnitudo); break;
							case 3: m.frontMagnitudo = Random.Range(minMagnitudo, maxMagnitudo); break;
							case 4: m.backLegLength = Random.Range(minLegLength, maxLegLenght); break;
							case 5: m.frontLegLength = Random.Range(minLegLength, maxLegLenght); break;
							case 6: m.frequencyFrequency = Random.Range(minFrequency, maxFrequency); break;
							case 7: m.color = Random.Range(minColor, maxColor); break;
						}
					}

					mutatedGenotypes.Add(m);

				}
				
				// Create the new population
				currentGenotypes = new List<Genotype>();
				currentGenotypes.AddRange(selectedGenotypes);
				currentGenotypes.AddRange(crossoverGenotypes);
				currentGenotypes.AddRange(mutatedGenotypes);

				Debug.Log($"Best genotype at generation {iGeneration} has fitness: {selectedGenotypes[0].fitness}");

				yield return null;
			}
			
			// Find the best individual
			var bestGenotype = currentGenotypes[0];
			Debug.LogError($"Best genotype is {bestGenotype}");
		}

		int Crossover(int v1, int v2)
		{
			return Random.value < 0.5f ? v1 : v2;
		}
		
	}

}
