﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace AI.Prediction.NGrams
{
	public class AINGram : MonoBehaviour, IPlayer
	{
		public int NGamesBeforePrediction = 10;

		private int nGames;

		public Text textNGramTable;
		public Text textWindow;
		
		void Awake()
		{
			for (int a1 = 1; a1 <= 3; a1++)
			{
				for (int a2 = 1; a2 <= 3; a2++)
				{
					var key = $"{(Action)a1}{(Action)a2}";
					var value = new int[3];
					nGramTable.Add(key, value);
				}
			}
		}
		
		public IEnumerator GetAction(Answer answer)
		{
			nGames++;
			if (nGames < NGamesBeforePrediction)
			{
				answer.action = (Action) Random.Range(1, 4);
			}
			else
			{
				// Let's look at the last two actions
				var a1 = window[0];
				var a2 = window[1];
				var key = $"{a1}{a2}";

				// Find the counts of actions inside the table
				int[] actionCounts = nGramTable[key];

				// Compute the probability of each opponent action
				int totalOpponentActions = actionCounts.Sum();
				float[] probabilityOfOpponentAction = new float[3];
				for (int i = 0; i < 3; i++)
				{
					probabilityOfOpponentAction[i] = actionCounts[i] * 1f/ totalOpponentActions;
				}
				
				// Choose randomly using those probability
				float rndChoice = Random.value;
				if (rndChoice < probabilityOfOpponentAction[(int)Action.S-1])
				{
					// I expect the other player to play S
					answer.action = Action.C;
				}
				else if (rndChoice < probabilityOfOpponentAction[(int) Action.S-1] + probabilityOfOpponentAction[(int) Action.C-1])
				{
					// I expect the other player to play C
					answer.action = Action.F;
				}
				else
				{
					// I expect the other player to play F
					answer.action = Action.S;
				}
			}
			Debug.LogWarning("We play " + answer.action);
			yield break;
		}
		
		List<Action> window = new List<Action>();
		Dictionary<string, int[]> nGramTable = new Dictionary<string, int[]>();

		public void ReceiveOpponentAction(Action opponentAction)
		{
			Debug.Log("Opponent played " + opponentAction);
			
			if (window.Count >= 2)
			{
				// Fill the table
				var a1 = window[0];
				var a2 = window[1];
				var key = $"{a1}{a2}";
				nGramTable[key][(int) opponentAction - 1] += 1;

				string s = "";
				for (int a = 1; a <= 3; a++)
				for (int b = 1; b <= 3; b++)
				{
					var k = $"{(Action) a}{(Action) b}";
					var sum = nGramTable[k].Sum();
					if (sum > 0) s += $"\n{k}: {(nGramTable[k][0] * 1f/sum):F} | {(nGramTable[k][1]*1f/sum):F} | {(nGramTable[k][2]*1f/sum):F}";
				}
				textNGramTable.text = s;
			}
			
			// Add to the queue
			window.Add(opponentAction);
			if (window.Count == 3) window.RemoveAt(0);
			
			if (window.Count >= 2) textWindow.text = $"{window[0]}{window[1]}";
		}
		
	}
}