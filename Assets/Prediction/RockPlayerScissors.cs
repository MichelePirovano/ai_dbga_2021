using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Prediction.NGrams
{
    public enum Action
    {
        S = 1,    // Sasso - Rock
        C = 2,    // Carta - Paper
        F = 3     // Forbici - Scissors
    }

    public class Answer
    {
        public Action action;
    }
    
    public interface IPlayer
    {
        IEnumerator GetAction(Answer answer);

        void ReceiveOpponentAction(Action opponentAction);
    }
    
    public class RockPlayerScissors : MonoBehaviour
    {
        public IPlayer pl1;
        public IPlayer pl2;

        public int MaxGames = 10;

        private int nGames = 0;

        private int nWon = 0;
        private int nLost = 0;
        private int nDraw = 0;
        
        IEnumerator Start()
        {
            var players = GetComponents<IPlayer>();
            pl1 = players[0];
            pl2 = players[1];
            
            while (nGames < MaxGames)
            {
                // Get the actions
                var answer1 = new Answer();
                yield return pl1.GetAction(answer1);
                var answer2 = new Answer();
                yield return pl2.GetAction(answer2);

                // Show the actions to the players
                pl1.ReceiveOpponentAction(answer2.action);
                pl2.ReceiveOpponentAction(answer1.action);
                
                // Determine the winner
                int diff = ((int) answer1.action - (int) answer2.action);
                if (diff == 1 || diff == -2)
                {
                    Debug.Log("PL1 wins");
                    nWon++;
                }
                else if (diff == 2 || diff == -1)
                {
                    Debug.Log("PL1 loses");
                    nLost++;
                }
                else
                {
                    Debug.Log("Draw");
                    nDraw++;
                }
                nGames++;
                Debug.Log("NGames: " + nGames);
                if (nGames % 20 == 0) yield return null;
            }

            float pl1Winrate = nWon * 100 / (float) nGames;
            float pl2Winrate = nLost * 100 / (float) nGames;
            
            Debug.Log($"PL1 winrate: {pl1Winrate} PL2 winrate: {pl2Winrate}");
            Debug.Log($"nWon {nWon} nLost {nLost} nDraw {nDraw}");
            
        }
    }

}
