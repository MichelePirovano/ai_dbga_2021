﻿using System.Collections;
using UnityEngine;

namespace AI.Prediction.NGrams
{
	public class AIRandom : MonoBehaviour, IPlayer
	{
		public Action BiasedAction;
		[Range(0, 1)] 
		public float BiasProbability = 0.1f;
		public bool RotateBias;    // If true, we'll rotate our Biased Action each time we are asked for an action

		
		public IEnumerator GetAction(Answer answer)
		{
			if (Random.value <= BiasProbability)
			{
				answer.action = BiasedAction;
				if (RotateBias)
				{
					BiasedAction += 1;
					if ((int) BiasedAction == 4) BiasedAction = (Action) 1;
				}
			}
			else
			{
				answer.action = (Action) Random.Range(1, 4);
			}
			yield break;
		}
		
		public void ReceiveOpponentAction(Action opponentAction)
		{
			// Do nothing, we ignore opponent actions
		}
	}
}