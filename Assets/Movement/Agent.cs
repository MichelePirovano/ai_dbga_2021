using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AI.Movement.Steering
{
    public class Agent : MonoBehaviour
    {
        public int Team;

        [Header("Acceleration")]
        public float linearAcceleration;
        public float angularAcceleration;
        
        [Header("Velocity")]
        public float currentLinearVelocity;
        public Vector2 linearVelocity;
        public float angularVelocity;

        [Header("Limits")]
        public float angleThreshold;
        public float velocityThreshold;
        public float stopThreshold;
        
        public float maxLinearAcceleration;
        public float maxAngularAcceleration;

        public float maxLinearVelocity;
        public float maxAngularVelocity;
        public bool StayStill { get; set; }

        private SteeringBehaviour[] steeringBehaviours;

        public Color Color
        {
            get
            {
                var color = Color.white;
                switch (Team)
                {
                    case 0:
                        color = Color.yellow;
                        break;
                    case 1:
                        color = Color.green;
                        break;
                    case 2:
                        color = Color.magenta;
                        break;
                    case 3:
                        color = Color.cyan;
                        break;
                }
                return color;
            }
        }


        void Start()
        {
            GetComponentInChildren<SpriteRenderer>().color = Color;
            
            AgentCollector.agents.Add(this);
            steeringBehaviours = GetComponents<SteeringBehaviour>();
        }

        public Vector2 AimingVector => transform.right;

        public float accelerationPercent;
        
        public void Update()
        {
            // Get the steering output
            Vector2 totalSteering = Vector2.zero;
            float totalWeight = 0;
            //Vector2 bestSteering = Vector2.zero;
            
            foreach (var steeringBehaviour in steeringBehaviours)
            {
                var output = steeringBehaviour.GetSteering();
                //var vector = output.targetLinearVelocityPercent*steeringBehaviour.Weight;
                //if (vector.sqrMagnitude > bestSteering.sqrMagnitude) bestSteering = vector;
                
                totalSteering += output.targetLinearVelocityPercent * steeringBehaviour.Weight;
                totalWeight += steeringBehaviour.Weight;
            }
            totalSteering /= totalWeight;
            
            
            
            
            var targetLinearVelocity = totalSteering * maxLinearVelocity;
            Debug.DrawLine(transform.position, transform.position + (Vector3)targetLinearVelocity * 1, Color.magenta);

            // Convert steering output into dynamic input (accelerations)
            if (targetLinearVelocity.sqrMagnitude > currentLinearVelocity * currentLinearVelocity + velocityThreshold)
            {
                linearAcceleration = maxLinearAcceleration;
            }
            else if (targetLinearVelocity.sqrMagnitude < currentLinearVelocity * currentLinearVelocity - velocityThreshold)
            {
                linearAcceleration = -maxLinearAcceleration;
            }
            else
            {
                linearAcceleration = 0;
            }
            
            // Angular acceleration
            Vector3 crossVector = Vector3.Cross(AimingVector, targetLinearVelocity);
            float angle = Mathf.Abs(Vector3.Angle(AimingVector, targetLinearVelocity));
            int rotationDirection = (int) Mathf.Sign(crossVector.z);

            accelerationPercent = 0;
            if (angle > angleThreshold) accelerationPercent = 1;
            else accelerationPercent = angle / angleThreshold;
            
            angularAcceleration = maxAngularAcceleration * rotationDirection * accelerationPercent;
            
            UpdateDynamics();
        }

        void UpdateDynamics()
        {
            // If too slow, just stop
            if (accelerationPercent == 0f && linearVelocity.sqrMagnitude < stopThreshold * stopThreshold)
            {
                currentLinearVelocity = 0;
                linearVelocity = Vector2.zero;
            }
            
            // Velocity update
            currentLinearVelocity += linearAcceleration * Time.deltaTime;
            currentLinearVelocity = Mathf.Clamp(currentLinearVelocity, -maxLinearVelocity, maxLinearVelocity);
            if (StayStill) currentLinearVelocity = 0;
            linearVelocity = AimingVector * currentLinearVelocity;

            angularVelocity += angularAcceleration * Time.deltaTime;
            angularVelocity = Mathf.Clamp(angularVelocity, -maxAngularVelocity, maxAngularVelocity);

            angularVelocity *= accelerationPercent; // Simulates some drag to avoid the pingpong of acceleration

            // Position / orientation update
            transform.position += (Vector3)linearVelocity * Time.deltaTime;
            transform.localEulerAngles +=  new Vector3(0,0,1) * (angularVelocity * Time.deltaTime);
            
            Debug.DrawLine(transform.position, transform.position + (Vector3)linearVelocity * 1, Color.green);
        }

        private void OnDestroy()
        {
            AgentCollector.agents.Remove(this);
        }




        public float GetClosestEnemyDistance(out Agent closestEnemyAgent)
        {		
            var minSqrDistance = Mathf.Infinity;
            closestEnemyAgent = null;
            var allAgents = AgentCollector.agents;
            foreach (var otherAgent in allAgents)
            {
                if (otherAgent == this) continue;
                if (otherAgent.Team == Team) continue;

                var distance = otherAgent.transform.position - transform.position;
                var sqrDistance = Vector2.SqrMagnitude(distance);
                if (sqrDistance < minSqrDistance)
                {
                    closestEnemyAgent = otherAgent;
                    minSqrDistance = sqrDistance;
                }
            }
            return Mathf.Sqrt(minSqrDistance);
        }
        
    }
}

