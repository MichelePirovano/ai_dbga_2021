﻿using System;
using System.Collections.Generic;
using AI.Movement.Pathfinding;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class FollowPathBehaviour : SteeringBehaviour
	{
		public AStarSearch pathfinder;
		public Node startNode;
		public Node endNode;
		
		public float ArrivalThreshold;

		private List<Edge> path;

		private Node currentNode;
		private int indexInPath;
		
		public override SteeringOutput GetSteering()
		{
			if (Input.GetKeyDown(KeyCode.A))
			{
				pathfinder.GraphMaker.Regenerate();
				path = null;
				pathfinder.IsExecuting = false;
				startNode = currentNode;
				currentNode = null;
			}

			
			// Start the pathfinder, if it has not started yet
			if (!pathfinder.IsExecuting)
			{
				pathfinder.StartNode = startNode;
				pathfinder.EndNode = endNode;
				path = new List<Edge>();
				pathfinder.FindPath(path);
			}

			SteeringOutput output = new SteeringOutput();
			
			// While the path is not ready, wait.
			if (path == null)
			{
				return output;
			}
			
			// Here we have the path defined
			if (currentNode == null)
			{
				indexInPath = 0;
				currentNode = startNode;
			}
			
			// Follow the current node until we are close enough
			output.targetLinearVelocityPercent = currentNode.transform.position - transform.position;
			output.targetLinearVelocityPercent.Normalize();
			if (Vector2.SqrMagnitude((Vector2)currentNode.transform.position- (Vector2)transform.position) < ArrivalThreshold * ArrivalThreshold)
			{
				// We finished the path
				if (indexInPath == path.Count)
				{
					return new SteeringOutput();
				}
				
				// Find the next node
				var edge = path[indexInPath];
				if (edge.NodeA == currentNode) currentNode = edge.NodeB;
				else if (edge.NodeB == currentNode) currentNode = edge.NodeA;
				indexInPath++;
			}
			
			return output;
		}

		public void OnDrawGizmos()
		{
			if (currentNode == null) return;
			Debug.DrawLine(transform.position, currentNode.transform.position, Color.yellow);
		}
	}
}