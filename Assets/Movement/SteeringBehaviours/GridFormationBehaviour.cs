﻿using UnityEditor;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class GridFormationBehaviour : SteeringBehaviour
	{
		public Transform Target;

		public Agent startAgent;
		
		public int width = 3;
		public float formationSpan = 1f;

		public int mySlot;	// To be different for each agent
		
		public float ArrivalThreshold;
		
		public override SteeringOutput GetSteering()
		{
			mySlot = AgentCollector.agents.IndexOf(GetComponent<Agent>());
			
			SteeringOutput output = new SteeringOutput();
			if (Target == null) return output;

			// Choose: follow the formation at this place, or follow the formation at the target position?
			var formationPos = (Vector2)startAgent.transform.position;// (Vector2)Target.position;

			var personalX = mySlot / width;
			var personalY = mySlot % width;
			var personalPos = formationPos + (Vector2.right * personalX + Vector2.up * personalY) * formationSpan;
			
			output.targetLinearVelocityPercent = personalPos - (Vector2)transform.position;
			if (output.targetLinearVelocityPercent.sqrMagnitude > 1) output.targetLinearVelocityPercent.Normalize();

			Debug.DrawLine(transform.position, personalPos, Color.white);
			
			if (Vector2.SqrMagnitude((Vector2)Target.position- (Vector2)transform.position) < ArrivalThreshold * ArrivalThreshold)
			{
				output.targetLinearVelocityPercent = Vector2.zero;
			}
			
			return output;
		}

	}
}