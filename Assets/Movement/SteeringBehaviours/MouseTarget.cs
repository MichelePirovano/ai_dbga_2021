using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class MouseTarget : MonoBehaviour
	{
		void Update()
		{
			var p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			p.z = 0;
			transform.position = p;
		}
		
	}

}