﻿using UnityEngine;

namespace AI.Movement.Steering
{
	public class FleeBehaviour : SteeringBehaviour
	{
		public Transform Target;

		public float FleeThreshold;
		
		public override SteeringOutput GetSteering()
		{
			SteeringOutput output = new SteeringOutput();
			if (Target == null) return output;
			
			var distance = transform.position - Target.position;
			distance.z = 0;
			
			var direction = distance.normalized;
			
			var magnitude = distance.magnitude;
			var magnitudeNormalized = magnitude / FleeThreshold;

			// Quadratic, to give a more realistic feeling
			magnitudeNormalized *= magnitudeNormalized;
			
			if (magnitudeNormalized > 1)
			{
				output.targetLinearVelocityPercent = Vector2.zero;
			}
			else
			{
				output.targetLinearVelocityPercent = direction * (1 - magnitudeNormalized);
			}
			
			return output;
		}
		
		
		public void OnDrawGizmos()
		{
			if (Target == null) return;
			var dist = transform.position - Target.position;
			dist.z = 0;
			Gizmos.color = Color.Lerp(Color.red, new Color(1,0,0,0), dist.magnitude / FleeThreshold);
			Gizmos.DrawWireSphere(Target.position, FleeThreshold);
		}
	}
}