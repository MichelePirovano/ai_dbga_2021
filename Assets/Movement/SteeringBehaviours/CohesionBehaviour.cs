﻿using UnityEngine;

namespace AI.Movement.Steering
{
	public class CohesionBehaviour : SteeringBehaviour
	{
		public float Radius;

		public override SteeringOutput GetSteering()
		{
			SteeringOutput output = new SteeringOutput();

			var baricenter = Vector2.zero;
			var nAgentsInCircle = 0;
			
			foreach (var agent in AgentCollector.agents)
			{
				if (agent == null || agent.gameObject == null) continue; // Skip dead agents
				if (agent.gameObject == gameObject) continue;	// Skip ourselves
				var distance = transform.position - agent.transform.position;
				if (distance.sqrMagnitude >= Radius*Radius) continue;	// Skip too far agents

				nAgentsInCircle += 1;

				baricenter += (Vector2)agent.transform.position;
			}

			if (nAgentsInCircle == 0) baricenter = Vector2.zero;
			else baricenter /= nAgentsInCircle;

			output.targetLinearVelocityPercent = baricenter - (Vector2)transform.position;
			output.targetLinearVelocityPercent = Vector2.ClampMagnitude(output.targetLinearVelocityPercent, 1);
			
			return output;
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(transform.position, Radius);
		}
	}
}