﻿using System;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class AlignmentBehaviour : SteeringBehaviour
	{
		public float Radius;

		public override SteeringOutput GetSteering()
		{
			SteeringOutput output = new SteeringOutput();

			var totalAiming = Vector2.zero;
			var nAgentsInCircle = 0;
			
			foreach (var agent in AgentCollector.agents)
			{
				if (agent == null || agent.gameObject == null) continue; // Skip dead agents
				if (agent.gameObject == gameObject) continue;	// Skip ourselves
				var distance = agent.transform.position - transform.position;
				if (distance.sqrMagnitude >= Radius*Radius) continue;	// Skip too far agents

				nAgentsInCircle += 1;
				
				var aiming = agent.AimingVector;
				totalAiming += aiming;
			}

			if (nAgentsInCircle == 0) totalAiming = Vector2.zero;
			else totalAiming /= nAgentsInCircle;

			output.targetLinearVelocityPercent = totalAiming;
			
			return output;
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(transform.position, Radius);
		}
	}
}