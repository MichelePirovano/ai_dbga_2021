﻿using System;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class SeekBehaviour : SteeringBehaviour
	{
		public Transform Target;

		public float ArrivalThreshold;
		
		public override SteeringOutput GetSteering()
		{
			SteeringOutput output = new SteeringOutput();
			if (Target == null) return output;
			output.targetLinearVelocityPercent = Target.position - transform.position;
			if (output.targetLinearVelocityPercent.sqrMagnitude > 1) output.targetLinearVelocityPercent.Normalize();

			if (Vector2.SqrMagnitude((Vector2)Target.position- (Vector2)transform.position) < ArrivalThreshold * ArrivalThreshold)
			{
				output.targetLinearVelocityPercent = Vector2.zero;
			}
			
			return output;
		}

		public void OnDrawGizmos()
		{
			if (Target == null) return;
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(Target.position, ArrivalThreshold);
		}
	}
}