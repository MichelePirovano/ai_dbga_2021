﻿using UnityEngine;

namespace AI.Movement.Steering
{
	public class ExampleSteeringBehaviour : SteeringBehaviour
	{
		public Vector2 TargetVelocityPercent;
		
		public override SteeringOutput GetSteering()
		{
			SteeringOutput output;
			output.targetLinearVelocityPercent = TargetVelocityPercent;
			return output;
		}
	}
}