﻿using UnityEngine;

namespace AI.Movement.Steering
{
	public struct SteeringOutput
	{
		public Vector2 targetLinearVelocityPercent;
	}
}