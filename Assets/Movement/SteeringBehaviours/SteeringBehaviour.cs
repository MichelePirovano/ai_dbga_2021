﻿using UnityEngine;

namespace AI.Movement.Steering
{
	public abstract class SteeringBehaviour : MonoBehaviour
	{
		[Range(0,10)]
		public float Weight = 1f;

		public abstract SteeringOutput GetSteering();
	}
}