﻿using System;
using UnityEngine;

namespace AI.Movement.Steering
{
	public class SeparationBehaviour : SteeringBehaviour
	{
		public float Radius;

		public override SteeringOutput GetSteering()
		{
			SteeringOutput output = new SteeringOutput();

			var totalSeparation = Vector2.zero;
			var nAgentsInCircle = 0;
			
			foreach (var agent in AgentCollector.agents)
			{
				if (agent == null || agent.gameObject == null) continue; // Skip dead agents
				if (agent.gameObject == gameObject) continue;	// Skip ourselves
				var distance = transform.position - agent.transform.position;
				if (distance.sqrMagnitude >= Radius*Radius) continue;	// Skip too far agents

				nAgentsInCircle += 1;

				var distMagnitudo = distance.magnitude;
				var distMagnitudoNormalized = distMagnitudo / Radius;
				var distMagniutodSqrNormalized = distMagnitudo * distMagnitudo / (Radius * Radius);
				var separation = (Vector2)distance.normalized * (1 - distMagniutodSqrNormalized);
				
				totalSeparation += separation;
			}

			if (nAgentsInCircle == 0) totalSeparation = Vector2.zero;
			else totalSeparation /= nAgentsInCircle;

			output.targetLinearVelocityPercent = totalSeparation;
			
			return output;
		}

		public void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(transform.position, Radius);
		}
	}
}