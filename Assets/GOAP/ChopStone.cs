﻿namespace AI.Decision.GOAP
{
	public class ChopStone : Action
	{
		public int Hardness = 1;
		public int Value = 4;
		
		void Awake()
		{
			preconditions["durabilityPick"] = current => current >= Hardness;
			effects["stone"] = +Value;
			effects["durabilityPick"] = -Hardness;
		}

		public override void Perform(Agent agent)
		{
			agent.currentStone += effects["stone"];
			agent.pickDurability += effects["durabilityPick"];
		}
	}
}