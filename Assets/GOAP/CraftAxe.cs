﻿namespace AI.Decision.GOAP
{
	public class CraftAxe : Action
	{
		public int AxeDurability = 5;
		public int WoodCost = 4;
		
		void Awake()
		{
			preconditions["wood"] = current =>  current >= WoodCost;
			preconditions["durability"] = current => current == 0;   
			
			effects["durability"] = +AxeDurability;
			effects["wood"] = -WoodCost;
		}
		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.axeDurability += effects["durability"];
		}
	}
}