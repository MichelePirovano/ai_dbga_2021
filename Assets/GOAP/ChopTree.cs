﻿namespace AI.Decision.GOAP
{
	public class ChopTree : Action
	{
		public int TreeHardness = 1;
		public int TreeWoodness = 4;
		
		void Awake()
		{
			preconditions["durability"] = current => current >= TreeHardness;
			effects["wood"] = +TreeWoodness;
			effects["durability"] = -TreeHardness;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.axeDurability += effects["durability"];
		}
	}
}