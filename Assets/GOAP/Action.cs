﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision.GOAP
{
	public abstract class Action : MonoBehaviour
	{
		public float Cost = 1f;

		// Setup action effects and preconditions for the planner
		public Dictionary<string, Func<int,bool>> preconditions = new Dictionary<string, Func<int,bool>>();
		public Dictionary<string, int> effects = new Dictionary<string, int>();

		// Action performed on the actual game
		public abstract void Perform(Agent agent);
	}
}