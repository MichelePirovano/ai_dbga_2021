﻿namespace AI.Decision.GOAP
{
	public class BuildHouse : Action
	{
		public int WoodCost = 4;
		public int StoneCost = 4;
		
		void Awake()
		{
			preconditions["wood"] = current =>  current >= WoodCost;
			preconditions["stone"] = current => current >= StoneCost;
			
			effects["wood"] = -WoodCost;
			effects["stone"] = -StoneCost;
			effects["houses"] = +1;
		}
		
		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.currentStone += effects["stone"];
			agent.currentHouses += effects["houses"];
		}
	}
}