using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Decision.GOAP
{
	public class Agent : MonoBehaviour
	{
		private List<Action> availableActions;

		public float Tick = 1f;
		public int MaxCost = 20;
		
		[Header("Goal")]
		public int targetWood = 0;
		public int targetStone = 0;
		public int targetHouses = 0;
		
		[Header("State")]
		public int currentWood = 0;
		public int currentStone = 0;
		public int axeDurability = 0;
		public int pickDurability = 0;
		public int currentHouses = 0;
		
		void Start()
		{
			availableActions = GetComponentsInChildren<Action>().ToList();

			StartCoroutine(Life());
		}

		private IEnumerator Life()
		{
			// Define a plan given my life objective
			var startState = new Dictionary<string,int>();
			startState["wood"] = currentWood;
			startState["stone"] = currentStone;
			startState["durability"] = axeDurability;
			startState["durabilityPick"] = pickDurability;
			startState["houses"] = currentHouses;
			
			var goalState = new Dictionary<string,int>();
			//goalState["wood"] = targetWood;
			//goalState["stone"] = targetStone;
			goalState["houses"] = targetHouses;

			var planner = new Planner();
			List<Action> plan = planner.GetPlan(availableActions, startState, goalState, MaxCost);

			// Keep performing actions
			while (true)
			{
				if (plan.Count > 0)
				{
					var chosenAction = plan[0];
					plan.RemoveAt(0);
					chosenAction.Perform(this);
					
					var s = $"Performing action {chosenAction.GetType().Name}";
					s += "\nW: " + currentWood;
					s += " Da: " + axeDurability;
					s += " Dp: " + pickDurability;
					s += " H: " + currentHouses;
					Debug.Log(s);

				}
				else
				{
					Debug.Log($"No action planned");
				}
				yield return new WaitForSeconds(Tick);
			}

		}
	}
	
}
