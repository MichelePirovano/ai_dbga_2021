﻿namespace AI.Decision.GOAP
{
	public class CollectBranches : Action
	{
		void Awake()
		{
			effects["wood"] = +1;
		}
		
		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
		}
	}
}