﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using UnityEngine;

namespace AI.Decision.GOAP
{
	public class Planner
	{
		private class Node
		{
			public Dictionary<string, int> state;
			public float totalCost; // Total cost to reach this node from the root
			
			public Node parent;
			public Action action; // Action to reach this node 

			public Node(Node _parent, Dictionary<string, int> _state, float _totalCost, Action _action)
			{
				parent = _parent;
				state = _state;
				totalCost = _totalCost;
				action = _action;
			}

		}
		
		public List<Action> GetPlan(List<Action> availableActions, Dictionary<string, int> startState, Dictionary<string, int> goalState, int maxCost)
		{
			// Given the start state and the available actions, we build a tree of all actions until we find the goal
			Node rootNode = new Node(null, startState, 0, null);
			PrintNode(rootNode);
			
			this.maxCost = maxCost;
			this.availableActions = availableActions;
			this.goalState = goalState;
			this.goalLeaves = new List<Node>();
			
			bool canFindPlan = BuildSubTree(rootNode);

			if (!canFindPlan)
			{
				// No plan could be found...
				return new List<Action>();
			}

			// Given the current tree, find the cheapest goal (leaf) state 
			Node cheapestGoal = null;
			foreach (var goalLeaf in goalLeaves)
			{
				if (cheapestGoal == null)
				{
					cheapestGoal = goalLeaf;
				}
				else
				{
					if (goalLeaf.totalCost < cheapestGoal.totalCost)
					{
						cheapestGoal = goalLeaf;
					}
				}
			}
			
			// We find the plan from the cheapest leaf back to the root 
			List<Action> plan = new List<Action>();
			Node n = cheapestGoal;
			while (n != null)
			{
				if (n.action != null)
				{
					plan.Insert(0, n.action);
				}
				n = n.parent;
			}
			
			Debug.Log($"Found a plan with total cost: {cheapestGoal.totalCost}");

			return plan;
		}

		private List<Action> availableActions;
		private List<Node> goalLeaves;
		private Dictionary<string, int> goalState;
		private int maxCost;
		

		private bool BuildSubTree(Node current)
		{
			// Guard against trees that are too deep
			if (current.totalCost >= maxCost)
			{
				return false;
			}
			
			// Guard against other goal leaves with a better path
			if (goalLeaves.Any(n => n.totalCost < current.totalCost))
			{
				return false;
			}
			
			bool foundSolution = false;
			foreach (var availableAction in availableActions)
			{
				// Check if the preconditions are fulfilled
				if (!MatchPreconditions(current.state, availableAction.preconditions)) continue;
				
				// We execute the action and find a new state
				var newState = ApplyAction(current.state, availableAction.effects);
				var newNode = new Node(current, newState, current.totalCost + availableAction.Cost, availableAction);
				PrintNode(newNode);

				if (MatchGoal(newState, goalState))
				{
					// We found the goal state!
					goalLeaves.Add(newNode);
					foundSolution = true;
				}
				else
				{
					if (BuildSubTree(newNode))
					{
						foundSolution = true;
					}
				}
			}
			return foundSolution;
		}

		private void PrintNode(Node newNode)
		{
			var s = $"C: {newNode.totalCost}";
			if (newNode.action != null) s += $" A{newNode.action.GetType().Name}";
			foreach (var stateKey in newNode.state.Keys)
			{
				s += $"\n{stateKey}[{newNode.state[stateKey]}]";
			}
			//Debug.Log(s);
		}

		private bool MatchGoal(Dictionary<string, int> currentState, Dictionary<string, int> goalState)
		{
			foreach (var key1 in goalState.Keys)
			{
				if (currentState[key1] < goalState[key1])
				{
					return false;
				}
			}
			return true;
		}

		private Dictionary<string,int> ApplyAction(Dictionary<string, int> currentState, Dictionary<string, int> effects)
		{
			// Copy the current state to avoid modifying the current one 
			var newState = new Dictionary<string, int>();
			foreach (var pair in currentState)
			{
				newState[pair.Key] = pair.Value;
			}
			
			// Apply the effects on the new state
			foreach (var effect in effects)
			{
				newState[effect.Key] += effect.Value;
			}

			return newState;
		}

		private bool MatchPreconditions(Dictionary<string, int> currentState, Dictionary<string, Func<int,bool>> preconditions)
		{
			foreach (var precondition in preconditions)
			{
				bool match;
				var currentValue = currentState[precondition.Key];
				match = precondition.Value(currentValue);
				if (!match) return false;
			}
			return true;
		}
	}
}