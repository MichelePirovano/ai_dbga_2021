﻿namespace AI.Decision.GOAP
{
	public class CraftPickaxe : Action
	{
		public int PickaxeDurability = 5;
		public int WoodCost = 4;
		public int StoneCost = 2;
		
		void Awake()
		{
			preconditions["wood"] = current =>  current >= WoodCost;
			preconditions["stone"] = current => current >= StoneCost;
			preconditions["durabilityPick"] = current => current == 0;   

			effects["durabilityPick"] = +PickaxeDurability;
			effects["wood"] = -WoodCost;
			effects["stone"] = -StoneCost;
		}
		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.currentStone += effects["stone"];
			agent.pickDurability += effects["durabilityPick"];
		}
	}
}