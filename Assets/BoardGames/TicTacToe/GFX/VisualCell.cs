﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AI.TicTacToe
{
    public class VisualCell : MonoBehaviour
    {
        public Image image;
        public Sprite xSprite;
        public Sprite oSprite;

        void OnValidate()
        {
            image = GetComponent<Image>();
        }
        
        public void SetValue(CellValue v)
        {
            image = GetComponent<Image>();
            image.enabled = v != CellValue.E;
            image.sprite = v == CellValue.X ? xSprite : oSprite;
        }
    }
}