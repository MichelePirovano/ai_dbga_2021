﻿using System.Collections;

namespace AI.TicTacToe
{
	public interface IIntelligence
	{
		IEnumerator GetBestAction(Game game, State currentState, Action outputAction);
	}
}