﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace AI.TicTacToe
{
	public class AIMiniMax : MonoBehaviour, IIntelligence
	{
		public VisualBoard[] AIBoard;
		
		public class StateScore
		{
			public int score;
		}
		
		public IEnumerator GetBestAction(Game game, State currentState, Action outputAction)
		{
			var stateScore = new StateScore();
			int depth = 0;
			yield return RecursiveMiniMaxing(game, currentState, outputAction, stateScore, depth);
			Debug.LogWarning("TOT TRAVERSED:" + totTraversed);
		}

		public float waitDelay = 0f;

		private int totTraversed;
		
		private IEnumerator RecursiveMiniMaxing(Game game, State currentState, Action outputAction, StateScore stateScore, int depth)
		{
			totTraversed++;
			//Debug.Log($"RecursiveMiniMaxing {depth} on state\n{currentState}");
			AIBoard[depth].ShowState(currentState, Color.Lerp(Color.magenta, Color.green, depth / 5f));
			yield return new WaitForSeconds(waitDelay);
			
			// If it is a terminal state, we return the score 
			if (game.IsTerminalState(currentState, out var winner))
			{
				if (winner == CellValue.Draw)
				{
					stateScore.score = 0;
				}
				else if (winner == currentState.player)
				{
					stateScore.score = 10;
				}
				else
				{
					stateScore.score = -10;
				}
				//Debug.Log("Terminal state - winner is " + winner);
				yield break;
			}
			
			
			// If it is not a terminal state...
			
			// We try all possible actions
			var possibleActions = game.GetPossibleActions(currentState);
			var bestScore = -100000;
			Action bestAction = null;
			// LOOP
			//Debug.Log($"Trying {possibleActions.Count()} actions");
			foreach (var possibleAction in possibleActions)
			{
				// For each action, get the next state
				var nextState = game.StateTransition(currentState, possibleAction);

				var tmpStateScore = new StateScore();
				// Call the recursive algorithm to get the score of that state
				yield return RecursiveMiniMaxing(game, nextState, outputAction, tmpStateScore, depth+1);

				tmpStateScore.score -= depth;
				
				// Since the score in the nextState is defined from the opponent, we negate the score
				tmpStateScore.score *= -1;
				
				// Keep the best score
				if (tmpStateScore.score > bestScore)
				{
					bestScore = tmpStateScore.score;
					bestAction = possibleAction;
				}
			}
			
			// We now have the best action
			outputAction.x = bestAction.x;
			outputAction.y = bestAction.y;
			outputAction.value = bestAction.value;
			
			// We also have the score of this state
			stateScore.score = bestScore;
			//Debug.LogError("Best action: " + outputAction + " best score: " + bestScore);
		}
	}
}