﻿using UnityEngine;

namespace AI.TicTacToe
{

	public class VisualBoard : MonoBehaviour
	{
		public VisualCell playerCell;
		private VisualCell[] cells;

		void OnValidate()
		{
			cells = GetComponentsInChildren<VisualCell>();
		}

		public void ShowState(State state)
		{
			ShowState(state, Color.white);
		}

		public void ShowState(State state, Color color)
		{
			cells = GetComponentsInChildren<VisualCell>();	// Slow, but forgive me
			playerCell.SetValue(state.player);
			
			for (int y = 0; y < 3; y++)
			{
				for (int x = 0; x < 3; x++)
				{
					int index = x + y * 3;
					cells[index].SetValue(state.cells[x, y]);
					cells[index].image.color = color;
				}
			}
		}


	}
}