﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.TicTacToe
{
    public enum CellValue
    {
        E = 0,    // Empty
        X = 1,
        O = -1,
        
        Draw = 2,
    }

    public class State
    {
        public CellValue player;
        public CellValue[,] cells = new CellValue[3,3];

        public override string ToString()
        {
            var s = "";
            for (int y = cells.GetLength(1)-1; y >= 0; y--)
            {
                for (int x = 0; x < cells.GetLength(0); x++)
                {
                    s += cells[x, y] + " | ";
                }
                s += "\n";
            }
            return s;
        }
    }

    public class Action
    {
        public int x;
        public int y;
        public CellValue value;

        public override string ToString()
        {
            return $"({x},{y}) pl{value}";
        }
    }

    public class Game : MonoBehaviour
    {
        public VisualBoard board;

        [Header("Initial State")]
        public CellValue initialPlayer;
        public CellValue[] initialBoard = new CellValue[9];

        public void OnValidate()
        {
            BuildInitialState();
        }

        private State BuildInitialState()
        {
            var state = new State();
            state.player = initialPlayer;
            for (int i = 0; i < initialBoard.Length; i++)
            {
                int x = i % 3;
                int y = i / 3;
                state.cells[x, y] = initialBoard[i];
            }
            board.ShowState(state);
            //Debug.LogError(state);
            return state;
        }
        

        IEnumerator Start()
        {
            var state = BuildInitialState();
            
            CellValue winner;
            // playing the game...
            while (!IsTerminalState(state, out winner))
            {
                // The player in turn chooses his action
                
                /////////// Our AI is here!
                IIntelligence intelligence = gameObject.GetComponent<IIntelligence>();
                Action action = new Action();
                yield return intelligence.GetBestAction(this, state, action);
                Debug.LogError("Chosen action: " + action);
                ////////////
                
                // The next state is found
                var nextState = StateTransition(state, action);
                
                // The next state becomes the current one
                state = nextState;
                
                board.ShowState(state, Color.green);
                yield return new WaitForSeconds(1.5f);
            }

            Debug.LogError("WINNER IS " + winner);
        }

        public IEnumerable<Action> GetPossibleActions(State s)
        {
            for (var x = 0; x < s.cells.GetLength(0); x++)
            for (var y = 0; y < s.cells.GetLength(1); y++)
            {
                var cellValue = s.cells[x, y];
                if (cellValue == CellValue.E)
                {
                    var a = new Action
                    {
                        x = x,
                        y = y,
                        value = s.player
                    };
                    yield return a;
                }
            }
        }

        public State StateTransition(State s1, Action a)
        {
            State s2 = new State();
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    s2.cells[x, y] = s1.cells[x, y];
                }
            }
            s2.cells[a.x, a.y] = a.value;
            s2.player = (CellValue) (-(int) s1.player);
            return s2;
        }
        
        public bool IsTerminalState(State s, out CellValue winner)
        {
            // Horizontal
            for (int y = 0; y < 3; y++)
            {
                if (s.cells[0, y] != CellValue.E
                    && s.cells[0, y] == s.cells[1, y]
                    && s.cells[0, y] == s.cells[2, y])
                {
                    winner = s.cells[0, y];
                    return true;
                }
            }
            
            // Vertical
            for (int x = 0; x < 3; x++)
            {
                if (s.cells[x, 0] != CellValue.E
                    && s.cells[x, 0] == s.cells[x, 1]
                    && s.cells[x, 0] == s.cells[x, 2])
                {
                    winner = s.cells[x, 0];
                    return true;
                }
            }
            
            // Diagonals
            if (s.cells[0, 0] != CellValue.E
                && s.cells[0, 0] == s.cells[1, 1] && s.cells[0, 0] == s.cells[2, 2])
            {
                winner = s.cells[0, 0];
                return true;
            }
            if (s.cells[2, 0] != CellValue.E
                && s.cells[2, 0] == s.cells[1, 1] && s.cells[2, 0] == s.cells[0, 2])
            {
                winner = s.cells[2, 0];
                return true;
            }

            // Check for draws
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (s.cells[x, y] == CellValue.E)
                    {
                        winner = CellValue.E;
                        return false;
                    }
                }
            }

            winner = CellValue.Draw;
            return true;
        }

    }

}
