﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace AI.TicTacToe
{
	public class AIRandom : MonoBehaviour, IIntelligence
	{
		public float ThinkingTime = 0f;
		
		public IEnumerator GetBestAction(Game game, State currentState, Action outputAction)
		{
			Action[] possibleActions = game.GetPossibleActions(currentState).ToArray();
			Action randomAction = possibleActions[Random.Range(0, possibleActions.Length)];
			outputAction.value = randomAction.value;
			outputAction.x = randomAction.x;
			outputAction.y = randomAction.y;
			yield return new WaitForSeconds(ThinkingTime);
		}
	}
}