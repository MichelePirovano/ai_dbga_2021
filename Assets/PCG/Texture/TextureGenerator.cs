using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.PCG.Texture
{
    

	public class TextureGenerator : MonoBehaviour
	{
		public int Width;
		public int Height;

		public int ScaleFactor;
		public int NOctaves;
		
		void Update()
		{
			var texture = new Texture2D(Width, Height, TextureFormat.RGBA32, false);
			texture.filterMode = FilterMode.Point;
			GetComponent<MeshRenderer>().material.mainTexture = texture;
			
			// Generate the pixels
			for (int x = 0; x < Width; x++)
			for (int y = 0; y < Height; y++)
			{
				//var color = new Color(x*1f/Width, y*1f/Height, 0f);
				//var value = Random.value;

				var value = 0f;
				for (int octave = 1; octave <= NOctaves; octave++)
				{
					var noise = Mathf.PerlinNoise(octave * x * 1f/ScaleFactor, octave * y * 1f/ScaleFactor);
					if (octave > 1) noise -= 0.5f;
					noise /= octave;
					value += noise;
				}

				var hue = 0f;
				if (value < 0.4f) hue = 256/360f;
				else if (value < 0.6f) hue = 114 / 360f;
				else hue = 26 / 360f;
				var color = Color.HSVToRGB(hue, 1, value);
				
				texture.SetPixel(x, y, color);
			}
			
			texture.Apply();

		}

	}

}