using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.PCG.Dungeon
{
	public class Cell : MonoBehaviour
	{
		public int x;
		public int y;
		private bool _isWall;
		
		public bool IsWall
		{
			get
			{
				return _isWall;
			}
			set
			{
				_isWall = value;
				if (_isWall) GetComponent<MeshRenderer>().material.color = Color.black;
				else GetComponent<MeshRenderer>().material.color = Color.white;
			}
		}

		public int Distance { get; set; }

		public void SetAsTarget()
		{
			GetComponent<MeshRenderer>().material.color = Color.green;
		}
		
		public void SetAsVisited()
		{
			//GetComponent<MeshRenderer>().material.color = Color.white;
			
			GetComponent<MeshRenderer>().material.color = Color.Lerp(Color.green, Color.red, Distance / 50f);

		}
		
	}
}
