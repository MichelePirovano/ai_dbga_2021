﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.PCG.Dungeon
{
	public enum Direction
	{
		N = 0,
		E = 1,
		S = 2,
		W = 3
	}
	
	public class DungeonGenerator : MonoBehaviour
	{
		public Cell PrefabCell;
		private Cell[,] cells;
		public int Width;
		public int Height;

		public bool UseFixedSeed;
		public int FixedSeed;
		
		void Start()
		{
			if (UseFixedSeed) Random.InitState(FixedSeed);
			
			cells = new Cell[Width,Height];
			for (int x = 0; x < Width; x++)
			for (int y = 0; y < Height; y++)
			{
				Cell cell = Instantiate(PrefabCell);
				cell.transform.position = new Vector3(x, y);
				cell.x = x;
				cell.y = y;
				cell.IsWall = true;
				cells[x, y] = cell;
			}

			StartCoroutine(AlgorithmCO());

		}

		public int startX;
		public int startY;

		private IEnumerator AlgorithmCO()
		{
			Stack<Cell> stack = new Stack<Cell>();
			stack.Push(cells[startX, startY]);

			HashSet<Cell> visitedFloors = new HashSet<Cell>();
			
			while (stack.Count > 0)
			{
				// Peek at the top cell of the stack
				var currentCell = stack.Peek();
				currentCell.IsWall = false;
				currentCell.Distance = stack.Count;
				currentCell.SetAsTarget();
				visitedFloors.Add(currentCell);
				yield return null;

				// Check the possible directions
				var possibleDirections = new List<Direction>();
				if (currentCell.y + 2 < Height && cells[currentCell.x, currentCell.y + 2].IsWall) possibleDirections.Add(Direction.N);
				if (currentCell.y - 2 >= 0 && cells[currentCell.x, currentCell.y - 2].IsWall) possibleDirections.Add(Direction.S);
				if (currentCell.x + 2 < Width && cells[currentCell.x + 2, currentCell.y].IsWall) possibleDirections.Add(Direction.E);
				if (currentCell.x - 2 >= 0 && cells[currentCell.x - 2, currentCell.y].IsWall) possibleDirections.Add(Direction.W);
			
				if (possibleDirections.Count == 0)
				{
					// If no directions are valid, we backtrack
					stack.Pop();
					currentCell.SetAsVisited();
				}
				else
				{
					// Else, choose a random direction among valid ones
					Direction chosenDir;
					int randIndex = Random.Range(0, possibleDirections.Count);
					chosenDir = possibleDirections[randIndex];

					// We dig to the new cell
					Cell toCell = null;
					Cell midCell = null;
					switch (chosenDir)
					{
						case Direction.N:
							midCell = cells[currentCell.x, currentCell.y + 1];
							toCell = cells[currentCell.x, currentCell.y + 2];
							break;
						case Direction.S:
							midCell = cells[currentCell.x, currentCell.y - 1];
							toCell = cells[currentCell.x, currentCell.y - 2];
							break;
						case Direction.E:
							midCell = cells[currentCell.x + 1, currentCell.y];
							toCell = cells[currentCell.x + 2, currentCell.y];
							break;
						case Direction.W:
							midCell = cells[currentCell.x - 1, currentCell.y];
							toCell = cells[currentCell.x - 2, currentCell.y];
							break;
					}

					currentCell.SetAsVisited();
					midCell.IsWall = false;
					toCell.IsWall = false;

					// Move to the new cell
					stack.Push(toCell);
				}
			}


			Cell farthestFloor = null;
			var maxDistance = -Mathf.Infinity;
			foreach (var visitedFloor in visitedFloors)
			{
				if (visitedFloor.Distance > maxDistance)
				{
					maxDistance = visitedFloor.Distance;
					farthestFloor = visitedFloor;
				}
			}

			farthestFloor.GetComponent<MeshRenderer>().material.color = Color.red;

		}
		
	}
}