using System.Collections;
using System.Collections.Generic;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision
{
    public class Shooter : MonoBehaviour
    {
        public GameObject bulletPrefab;

        public float RifleLength = 1f;
        public float ShootRate = 1f;

        public void Shoot()
        {
            var bulletGo = Instantiate(bulletPrefab);
            bulletGo.transform.position = transform.position + transform.right * RifleLength;
            bulletGo.transform.rotation = transform.rotation;

            GetComponentInChildren<SpriteRenderer>().color = GetComponent<Agent>().Color;
        }

        private bool isShooting;

        public void StartShooting()
        {
            if (isShooting) return;
            isShooting = true;
            InvokeRepeating("Shoot", 0f, ShootRate);
        }

        public void StopShooting()
        {
            if (!isShooting) return;
            isShooting = false;
            CancelInvoke("Shoot");
        }

    }
}
