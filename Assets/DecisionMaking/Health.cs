﻿using UnityEngine;

namespace AI.Decision
{
	public class Health : MonoBehaviour
	{
		public float Current = 10;
		public float RegenRate = 1f;
		
		void Start()
		{
			InvokeRepeating("Regen",RegenRate,RegenRate);
		}

		void Regen()
		{
			Current += 1;
			if (Current >= 10) Current = 10;
		}
		
		void OnTriggerEnter2D(Collider2D other)
		{
			if (other.GetComponent<Bullet>())
			{
				Destroy(other.gameObject); 
				Current--;
				Debug.Log($"Remaining health: {Current}");

				if (Current == 0)
				{
					Destroy(gameObject);
				}
			}
		}
	}
}