using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AI.Decision
{
	public abstract class DecisionMaker : MonoBehaviour
	{
		public abstract void MakeDecision();
	}
	
	public class Brain : MonoBehaviour
	{
		public float tickDelay = 1f;

		public DecisionMaker decisionMaker;
		
		void Start()
		{
			decisionMaker = GetComponent<DecisionMaker>();
			InvokeRepeating("TickBrain", tickDelay, tickDelay);
		}

		void TickBrain()
		{
			decisionMaker.MakeDecision();
		}
		
		
		
	}

}
