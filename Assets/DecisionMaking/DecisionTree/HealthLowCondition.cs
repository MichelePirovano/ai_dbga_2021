﻿namespace AI.Decision.DecisionTree
{
	public class HealthLowCondition : Decision
	{
		public int HealthThreshold;
		public override bool CheckCondition()
		{
			return MyBrain.GetComponent<Health>().Current <= HealthThreshold;
		}
	}
}