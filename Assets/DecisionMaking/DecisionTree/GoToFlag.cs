﻿using System.Linq;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.DecisionTree
{
	
	public class GoToFlag : Action
	{
		public bool GoToEnemyFlag;
		
		public override void PerformAction()
		{
			var seek = MyBrain.GetComponent<SeekBehaviour>();
			var flee = MyBrain.GetComponent<FleeBehaviour>();

			var flags = GameObject.FindObjectsOfType<Flag>();

			if (GoToEnemyFlag) seek.Target = flags.First(f => f.Team != MyBrain.GetComponent<Agent>().Team).transform;
			else seek.Target = flags.First(f => f.Team == MyBrain.GetComponent<Agent>().Team).transform;
			
			seek.Weight = 1f;
			flee.Weight = 0f;
			
			MyBrain.GetComponent<Agent>().StayStill = false;
			MyBrain.GetComponent<Shooter>().StopShooting();
		}
	}
}