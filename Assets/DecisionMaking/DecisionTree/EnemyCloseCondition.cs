﻿using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.DecisionTree
{
	public class EnemyCloseCondition : Decision
	{
		public float MinDistance;
		
		public override bool CheckCondition()
		{
			var allAgents = AgentCollector.agents;
			
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == MyBrain.GetComponent<Agent>()) continue;
				if (otherAgent.Team == MyBrain.GetComponent<Agent>().Team) continue;

				var distance = otherAgent.transform.position - MyBrain.transform.position;
				if (Vector2.SqrMagnitude(distance) < MinDistance * MinDistance)
				{
					return true;
				}
			}

			return false;
		}
	}
}