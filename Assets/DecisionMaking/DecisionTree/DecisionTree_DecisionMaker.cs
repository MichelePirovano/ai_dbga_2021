using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace  AI.Decision.DecisionTree
{
	public abstract class Node : MonoBehaviour
	{
		void OnValidate()
		{
			this.name = GetType().Name;
			GetComponentInChildren<TextMesh>().text = GetType().Name;
		}
		
		
		public Brain MyBrain;
		
		public abstract void MakeDecision();

		public virtual void Reset(Brain brain)
		{
			MyBrain = brain;
			GetComponentInChildren<SpriteRenderer>().color = Color.white;
		}
	}

	public abstract class Action : Node
	{
		public override void MakeDecision()
		{
			GetComponentInChildren<SpriteRenderer>().color = Color.yellow;
			PerformAction();
		}

		public abstract void PerformAction();
	}


	public abstract class Decision : Node
	{
		public Node trueNode;
		public Node falseNode;

		private void OnDrawGizmos()
		{
			if (trueNode != null) Debug.DrawLine(transform.position, trueNode.transform.position, Color.green);
			if (falseNode != null) Debug.DrawLine(transform.position, falseNode.transform.position, Color.magenta);
		}

		public abstract bool CheckCondition();
		
		public override void MakeDecision()
		{
			if (CheckCondition())
			{
				trueNode.MakeDecision();
			}
			else
			{
				falseNode.MakeDecision();
			}
		}

		public override void Reset(Brain brain)
		{
			base.Reset(brain);
			if (trueNode != null) trueNode.Reset(brain);
			if (falseNode != null) falseNode.Reset(brain);
		}
	}

	public class DecisionTree_DecisionMaker : DecisionMaker
	{
		public Node rootNode;
		
		public override void MakeDecision()
		{
			rootNode.Reset(GetComponent<Brain>());
			rootNode.MakeDecision();
		}
	}

}
