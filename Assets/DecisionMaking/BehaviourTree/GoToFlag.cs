﻿using System.Linq;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class GoToFlag : Task
	{
		public bool GoToEnemyFlag;

		protected override TaskState InternalRun()
		{
			var seek = MyBrain.GetComponent<SeekBehaviour>();
			var flee = MyBrain.GetComponent<FleeBehaviour>();

			var flags = GameObject.FindObjectsOfType<Flag>();

			if (GoToEnemyFlag) seek.Target = flags.First(f => f.Team != MyBrain.GetComponent<Agent>().Team).transform;
			else seek.Target = flags.First(f => f.Team == MyBrain.GetComponent<Agent>().Team).transform;
			
			seek.Weight = 1f;
			flee.Weight = 0f;
			
			MyBrain.GetComponent<Agent>().StayStill = false;
			MyBrain.GetComponent<Shooter>().StopShooting();

			// @note: we should actually Wait until a condition is met
			GetComponentInChildren<SpriteRenderer>().color = Color.green;
			return TaskState.Success;
		}
	}
}