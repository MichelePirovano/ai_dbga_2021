﻿using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class Selector : Task
	{
		protected override TaskState InternalRun()
		{
			foreach (var child in children)
			{
				if (child.Run() == TaskState.Success)
				{
					GetComponentInChildren<SpriteRenderer>().color = Color.green;
					return TaskState.Success;
				}
			}
			GetComponentInChildren<SpriteRenderer>().color = Color.magenta;
			return TaskState.Failure;
		}
	}
}