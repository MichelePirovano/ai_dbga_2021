﻿using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class Sequence : Task
	{
		protected override TaskState InternalRun()
		{
			foreach (var child in children)
			{
				if (child.Run() == TaskState.Failure)
				{
					GetComponentInChildren<SpriteRenderer>().color = Color.magenta;
					return TaskState.Failure;
				}
			}
			GetComponentInChildren<SpriteRenderer>().color = Color.green;
			return TaskState.Success;
		}
	}
}