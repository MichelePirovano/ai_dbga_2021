﻿using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class ShootEnemy : Task
	{
		protected override TaskState InternalRun()
		{
			// Get enemy
			var minSqrDistance = Mathf.Infinity;
			Agent closestEnemyAgent = null;
			var allAgents = AgentCollector.agents;
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == MyBrain.GetComponent<Agent>()) continue;
				if (otherAgent.Team == MyBrain.GetComponent<Agent>().Team) continue;

				var distance = otherAgent.transform.position - MyBrain.transform.position;
				var sqrDistance = Vector2.SqrMagnitude(distance);
				if (sqrDistance < minSqrDistance)
				{
					closestEnemyAgent = otherAgent;
					minSqrDistance = sqrDistance;
				}
			}
			
			// Look at enemy
			var seek = MyBrain.GetComponent<SeekBehaviour>();
			var flee = MyBrain.GetComponent<FleeBehaviour>();
			seek.Target = closestEnemyAgent.transform;
			seek.Weight = 1f;
			flee.Weight = 0f;
			
			// Set max speed at zero so we just look
			MyBrain.GetComponent<Agent>().StayStill = true;
			
			// Let's kill the #*@**@*
			MyBrain.GetComponent<Shooter>().StartShooting();

			GetComponentInChildren<SpriteRenderer>().color = Color.green;
			return TaskState.Success;
		}
	}
}