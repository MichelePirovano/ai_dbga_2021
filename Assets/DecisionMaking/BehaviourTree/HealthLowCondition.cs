﻿using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class HealthLowCondition : Task
	{
		public int HealthThreshold;

		protected override TaskState InternalRun()
		{
			if (MyBrain.GetComponent<Health>().Current <= HealthThreshold)
			{
				GetComponentInChildren<SpriteRenderer>().color = Color.green;
				return TaskState.Success;
			}
			else
			{
				GetComponentInChildren<SpriteRenderer>().color = Color.magenta;
				return TaskState.Failure;
			}
		}
	}
}