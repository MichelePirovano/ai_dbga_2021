﻿using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public class EnemyCloseCondition : Task
	{
		public float MinDistance;

		protected override TaskState InternalRun()
		{
			var allAgents = AgentCollector.agents;
			
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == MyBrain.GetComponent<Agent>()) continue;
				if (otherAgent.Team == MyBrain.GetComponent<Agent>().Team) continue;

				var distance = otherAgent.transform.position - MyBrain.transform.position;
				if (Vector2.SqrMagnitude(distance) < MinDistance * MinDistance)
				{
					GetComponentInChildren<SpriteRenderer>().color = Color.green;
					return TaskState.Success;
				}
			}

			GetComponentInChildren<SpriteRenderer>().color = Color.magenta;
			return TaskState.Failure;
		}
	}
}