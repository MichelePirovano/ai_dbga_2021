using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision.BehaviourTree
{
	public enum TaskState
	{
		Failure = 0,
		Success = 1,
		Wait
	}

	public abstract class Task : MonoBehaviour
	{
		public Brain MyBrain;
		
		public List<Task> children = new List<Task>();

		void OnValidate()
		{
			this.name = GetType().Name;
			GetComponentInChildren<TextMesh>().text = GetType().Name;
		}
		
		private void OnDrawGizmos()
		{
			foreach (var child in children)
			{
				Debug.DrawLine(transform.position, child.transform.position, Color.magenta);
			}
		}

		protected abstract TaskState InternalRun();

		public TaskState Run()
		{
			GetComponentInChildren<SpriteRenderer>().color = Color.yellow;
			return InternalRun();
		}
		
		public void Reset(Brain brain)
		{
			MyBrain = brain;
			foreach (var child in children) child.Reset(brain);
			GetComponentInChildren<SpriteRenderer>().color = Color.white;
		}
	}

	public class BehaviourTree_DecisionMaker : DecisionMaker
	{
		public Task Root;
		
		public override void MakeDecision()
		{
			Root.Reset(GetComponent<Brain>());
			Root.Run();
		}
	}

}