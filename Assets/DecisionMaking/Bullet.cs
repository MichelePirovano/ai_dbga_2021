﻿using UnityEngine;

namespace AI.Decision
{
	public class Bullet : MonoBehaviour
	{
		public float Speed = 1f;
        
		private void Update()
		{
			transform.position += transform.right * (Time.deltaTime * Speed);
		}
        
	}
}