using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AI.Decision;
using AI.Decision.DecisionTree;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.FSM
{
	public abstract class State
	{
		public abstract void OnEnter();
		public abstract void Handle(); // Called every Update
		public abstract void OnExit();
	}

	public class StateExample : State
	{
		public override void OnEnter()
		{
			
		}
		
		public override void Handle()
		{
			
		}

		public override void OnExit()
		{
		}
	}
	

	public class Transition
	{
		public string command; // Could be an Enum
		public State currentState;
		public State nextState;
	}

	public class FSM_NoAnimator : DecisionMaker
	{
		public List<State> states = new List<State>();
		public List<Transition> transitions = new List<Transition>();
		public State currentState;
		
		public string chosenCommand = "";

		private bool initialised;

		public void Initialise()
		{
			if (initialised) return;
			initialised = true;
			
			states.Add(new StateExample());
			states.Add(new StateExample());
			
			transitions.Add(new Transition{command = "A", currentState = states[0], nextState = states[1]});

			currentState = states[0];
			currentState.OnEnter();
		}
		
		public override void MakeDecision()
		{
			Initialise();
			
			// Gather inputs and traverse transitions
			foreach (var transition in transitions)
			{
				if (transition.command == chosenCommand && currentState == transition.currentState)
				{
					currentState.OnExit();
					currentState = transition.nextState;
					currentState.OnEnter();
				}
			}

		}

		public void Update()
		{
			Initialise();
			currentState.Handle();
		}
	}
    
}
