﻿using System.Linq;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.FSM
{
	public class FSMShootEnemy : StateMachineBehaviour
	{
		SeekBehaviour seek;
		FleeBehaviour flee;
		Brain MyBrain;

		void ChooseTarget()
		{
			// Get enemy
			var minSqrDistance = Mathf.Infinity;
			Agent closestEnemyAgent = null;
			var allAgents = AgentCollector.agents;
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == MyBrain.GetComponent<Agent>()) continue;
				if (otherAgent.Team == MyBrain.GetComponent<Agent>().Team) continue;

				var distance = otherAgent.transform.position - MyBrain.transform.position;
				var sqrDistance = Vector2.SqrMagnitude(distance);
				if (sqrDistance < minSqrDistance)
				{
					closestEnemyAgent = otherAgent;
					minSqrDistance = sqrDistance;
				}
			}
			
			seek.Target = closestEnemyAgent.transform;
		}

		private bool isInside = false;
		
		// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
		public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			isInside = true;
			MyBrain = animator.gameObject.GetComponent<Brain>();
			
			// Look at enemy
			seek = MyBrain.GetComponent<SeekBehaviour>();
			flee = MyBrain.GetComponent<FleeBehaviour>();
			ChooseTarget();
			
			seek.Weight = 1f;
			flee.Weight = 0f;
			
			MyBrain.GetComponent<Agent>().StayStill = true;
			MyBrain.GetComponent<Shooter>().StartShooting();
		}

		// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
		public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (isInside) ChooseTarget();
		}

		// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
		public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			isInside = false;
			MyBrain.GetComponent<Agent>().StayStill = false;
			MyBrain.GetComponent<Shooter>().StopShooting();
		}
	}
}