using System.Collections;
using System.Collections.Generic;
using AI.Decision;
using AI.Decision.DecisionTree;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.FSM
{
	public class FSM_DecisionMaker : DecisionMaker
	{
		public Animator aiAnimatorFSM;
		
		public override void MakeDecision()
		{
			var myHealth = GetComponent<Health>().Current;
			aiAnimatorFSM.SetInteger("MyHealth", (int)myHealth);

			var closestEnemyDistance = GetComponent<Agent>().GetClosestEnemyDistance(out var closestEnemyAgent);
			aiAnimatorFSM.SetFloat("ClosestEnemyDistance", closestEnemyDistance);
		}
	}

	
}
