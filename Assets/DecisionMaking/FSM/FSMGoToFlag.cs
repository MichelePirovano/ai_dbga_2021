using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AI.Decision;
using AI.Movement.Steering;
using UnityEngine;

namespace AI.Decision.FSM
{
    public class FSMGoToFlag : StateMachineBehaviour
    {
        public bool GoToEnemyFlag;
        SeekBehaviour seek;
        FleeBehaviour flee;
        Brain MyBrain;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            MyBrain = animator.gameObject.GetComponent<Brain>();
            seek = MyBrain.GetComponent<SeekBehaviour>();
            flee = MyBrain.GetComponent<FleeBehaviour>();

            var flags = GameObject.FindObjectsOfType<Flag>();
            if (GoToEnemyFlag) seek.Target = flags.First(f => f.Team != MyBrain.GetComponent<Agent>().Team).transform;
            else seek.Target = flags.First(f => f.Team == MyBrain.GetComponent<Agent>().Team).transform;

            seek.Weight = 1f;
            flee.Weight = 0f;
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

        }
    }
}
